#!/usr/bin/env bash

pip3 list | grep cloudtoken | cut -f 1 -d ' ' | xargs pip3 uninstall -y
