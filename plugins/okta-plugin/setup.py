from setuptools import setup
import os

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='cloudtoken-plugin.okta',
    version="0.0.7",
    description='OKTA plugin for cloudtoken.',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://bitbucket.org/atlassian/cloudtoken',
    author='Andy Loughran',
    author_email='andy@lockran.com',
    license='Apache',
    py_modules=['cloudtoken_plugin.okta'],
    zip_safe=False,
    python_requires='>=3.5',
    install_requires=[
        "cloudtoken>=0.0.1",
        "requests>=2.18.4",
        "bs4>=0.0.1",
        ],
)
